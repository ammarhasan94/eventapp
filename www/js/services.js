angular.module('starter.services', [])
    // .constant("Domain","http://event-admin.herokuapp.com")
    .constant("Domain", "https://shrouded-temple-6391.herokuapp.com")
    // .constant("Domain", "http://192.168.10.7:9000")
   
    .constant("EventCode", "A")    
    
    .constant("CLOUDINARY", {
        "UPLOAD_PRESET": "w1l1b9eg",
        "URL": "https://api.cloudinary.com/v1_1/drsxe8hvl/image/upload"
    })
    
    .factory('Users', function($http, Domain) {
        return {
            create: function(userData) {
                return $http.post(Domain + '/api/user', userData)
                    .then(function(response) {
                        return response;
                        // invalid response

                    }, function(response) {
                        // something went wrong
                        return response
                    });
            },
            getUserById : function(userId) {
                return $http.get(Domain + '/api/user/'+ userId)
                    .then(function(response) {
                        return response;
                    }, function(err) {
                        return err
                    });
        }
    }
})

.factory('Events', function($http, Domain) {
    return {
        joinEvent: function(code_userId) {
            return $http.post(Domain + '/api/joinEvent', code_userId)
                .then(function(response) {
                    return response;
                }, function(response) {
                    return response
                });
        },
        getEventByCode: function(code) {
            return $http.get(Domain + '/api/events/code/' + code)
                .then(function(response) {
                    return response;
                }, function(err) {
                    return err
                });
        },
        uploadimage: function(base64URL) {
            return $http.post(Domain + '/uploadimage', {
                    img: base64URL
                })
                .then(function(response) {
                    return response;
                }, function(response) {
                    return response;
                });
        },
        createPicture: function(url, userId, eventId,description) {
            return $http.post(Domain + '/api/pictures', {
                    url: url,
                    userId: userId,
                    eventId: eventId,
                    description: description
                })
                .then(function(response) {
                    return response;
                }, function(response) {
                    return response
                });
        }
    }
})

.factory('FB_Google', function($http, $localStorage) {

    var fb = function() {
        if ($localStorage.hasOwnProperty("fbAccessToken") === true) {
            $http.get("https://graph.facebook.com/v2.4/me", {
                    params: {
                        access_token: $localStorage.fbAccessToken,
                        fields: "name,email",
                        format: "json"
                    }
                })
                .then(function(result) {
                    console.log(result.data);
                    return result.data;
                }, function(error) {
                    alert("There was a problem getting your profile.  Check the logs for details.");
                    console.log(error);
                });
        }
    };
    var gplus = function() {
        if ($localStorage.hasOwnProperty("googleAccessToken") === true) {
            $http.get("https://www.googleapis.com/plus/v1/people/me", {
                    params: {
                        access_token: $localStorage.googleAccessToken,
                        fields: "displayName,emails",
                        format: "json"
                    }
                })
                .then(function(result) {
                    console.log(result.data);
                    return result.data;
                }, function(error) {
                    alert("There was a problem getting your profile.  Check the logs for details.");
                    console.log(error);
                });
        }

    };
    return {
        fbData: fb,
        googleData: gplus
    }
})

.factory('Gallery', function($http,Domain) {
    var AllImages = [];
    var selectedImage =0;

    return {
        getSelected: function() {
            return selectedImage;
        },
        setSelected: function(index) {
            selectedImage = index;
        },
        getNext: function() {
            if (AllImages[selectedImage.id + 1] != null) {
                selectedImage = AllImages[selectedImage.id + 1];
                return selectedImage;
            }
            return null;
        },
        getPrevious: function() {
            if (AllImages[selectedImage.id - 1] != null) {
                selectedImage = AllImages[selectedImage.id - 1];
                return selectedImage;
            }
            return null;
        },
        getLoadedPictures:function(){
            return AllImages;  
        },
        getApprovedPictures: function(eventId) {
            return $http.get(Domain + '/api/pictures/approved/' + eventId)
                .then(function(response) {
                    AllImages=response.data;
                    console.log(AllImages);
                    return AllImages;
                }, function(error) {
                    alert("There was a problem getting pictures");
                    return error;
                });
        },
        getAllPictures: function(eventId) {
            return $http.get(Domain + '/api/pictures/event/' + eventId)
                .then(function(response) {
                    AllImages=response.data;
                    console.log(AllImages);
                    return AllImages;
                }, function(error) {
                    alert("There was a problem getting pictures");
                    return error;
                });
        },
        approvePicture: function (userId, pictureId) {
            return $http.put(Domain + '/api/pictures/'+pictureId+'/approve/user/'+userId)
                .then(function (response) {
                    return response;
                }, function (response) {
                    return response;
                });
        },
        disapprovePicture: function (userId, pictureId) {
            return $http.put(Domain + '/api/pictures/'+pictureId+'/disapprove/user/'+userId)
                .then(function (response) {
                    return response;
                }, function (response) {
                    return response;
                });
        }
    };
})

.factory('Camera', ['$q', function($q) {

    return {
        getPicture: function(options) {
            var q = $q.defer();
            navigator.camera.getPicture(function(result) {
                // Do any magic you need
                q.resolve(result);
            }, function(err) {
                alert(err);
                q.reject(err);
            }, options);

            return q.promise;
        }
    }
}])

.factory('Like', function($http,Domain) {
    var AllLikes = [];
    
    return {
        pushToAllLikes:function(like){
            AllLikes.push(like);
        },
        create: function (pictureId, userId) {
            console.log(Domain + '/api/likes/' + pictureId + '/user/' + userId);
            return $http.post(Domain + '/api/likes', { userId: userId, pictureId: pictureId }).then(function (response) {
                return response;
            }, function (response) {
                alert("There was a problem liking picture");
                return response;
            });
        },
        getCount:function(pictureId){
            return $http.get(Domain + '/api/count/likes/' + pictureId)
                .then(function (response) {
                    return response;
                }, function (error) {
                    alert("There was a problem getting likes count");
                    return error;
                });
        },
        getByPictureId: function (pictureId) {
            console.log(Domain + '/api/likes/' + pictureId);
            return $http.get(Domain + '/api/likes/' + pictureId)
                .then(function (response) {
                    return response;
                }, function (error) {
                    alert("There was a problem getting likes");
                    return error;
                });
        },
        getAllLikes:function(){
            return AllLikes;  
        },
        getByPictureIdAndUserId: function (pictureId, userId) {
            console.log(Domain + '/api/likes/' + pictureId + '/user/' + userId);
            return $http.get(Domain + '/api/likes/' + pictureId + '/user/' + userId)
                .then(function (response) {
                    return response;
                }, function (error) {
                    alert("There was a problem getting likes");
                    return error;
                });
        },
        delete: function (pictureId,userId) {
            return $http.delete(Domain + '/api/likes/' + pictureId + '/user/' + userId)
                .then(function (response) {
                    return response;
                }, function (error) {
                    alert("There was a problem deleting likes");
                    return error;
                });
        }
        
    }
})
.factory('Dislike', function($http,Domain) {
    return {
        create: function (pictureId, userId) {
            console.log(Domain + '/api/dislikes/' + pictureId + '/user/' + userId);
            return $http.post(Domain + '/api/dislikes', { userId: userId, pictureId: pictureId }).then(function (response) {
                return response;
            }, function (response) {
                alert("There was a problem disliking picture");
                return response;
            });
        },
        getCount:function(pictureId){
            return $http.get(Domain + '/api/count/dislikes/' + pictureId)
                .then(function (response) {
                    return response;
                }, function (error) {
                    alert("There was a problem getting dislikes count");
                    return error;
                });
        },
        getByPictureId: function (pictureId) {
            console.log(Domain + '/api/dislikes/' + pictureId);
            return $http.get(Domain + '/api/dislikes/' + pictureId)
                .then(function (response) {
                    return response;
                }, function (error) {
                    alert("There was a problem getting dislikes");
                    return error;
                });
        },
        getByPictureIdAndUserId: function (pictureId, userId) {
            console.log(Domain + '/api/dislikes/' + pictureId + '/user/' + userId);
            return $http.get(Domain + '/api/dislikes/' + pictureId + '/user/' + userId)
                .then(function (response) {
                    return response;
                }, function (error) {
                    alert("There was a problem getting dislikes");
                    return error;
                });
        },
        delete: function (pictureId,userId) {
            return $http.delete(Domain + '/api/dislikes/' + pictureId + '/user/' + userId)
                .then(function (response) {
                    return response;
                }, function (error) {
                    alert("There was a problem deleting dislikes");
                    return error;
                });
        }
        
    }
})
.factory('EventMember', function($http, Domain) {
        return {
            create: function(eventId,userId) {
                return $http.post(Domain + '/api/eventMembers',{eventId:eventId,userId:userId})
                    .then(function(response) {
                        return response;
                        // invalid response

                    }, function(response) {
                        // something went wrong
                        return response
                    });
            },
            getByEventIdAndUserId : function(eventId,userId) {
                return $http.get(Domain + '/api/eventMembers/'+eventId+'/user/'+ userId)
                    .then(function(response) {
                        return response;
                    }, function(err) {
                        return err
                    });
        }
    }
});