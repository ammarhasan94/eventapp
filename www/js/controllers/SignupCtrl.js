angular.module('starter')

	.controller('SignupCtrl', function ($scope, $state, $http, $cordovaOauth, $localStorage,$rootScope,$ionicLoading, Users, FB_Google, Events,EventCode) 
    {

        // Get Event by Event Code
        Events.getEventByCode(EventCode)
            .then(function (response2) {
                console.log(response2);
                if (response2.status == 200) {
                    $localStorage.event = response2.data;
                    $scope.event=response2.data[0];
                }
            }, function (err) {
                console.log(err);
            });
        
        // //Join event
        function JoinEvent(){
            Events.joinEvent({userId: $localStorage.user._id,code: EventCode})
                .then(function (response) {
                    console.log(response);
                    $localStorage.role = response.data.role;

                    if (response.status == 200) {
                      
                                                                                
                        //broadcast the event to all child scopes
                        $rootScope.$broadcast('changeEvent', { data: "Dummy Data" });
                        console.log("Broadcast 'changeEvent' ");
                        $ionicLoading.hide();

                        $state.go('tab.home');



                    } else if (response.status == 404) {
                        $ionicLoading.hide();
                        alert("Please enter valid Event Code");
                    } else {
                        $ionicLoading.hide();
                        alert("Error joining event");
                    }

                }, function (err) {
                    console.log(err);
                });
        
        }
        
        

        $scope.facebookSignup = function () {
            console.log(window.history);
            $ionicLoading.show({
                    content: 'Uploading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200
                    // showDelay: 500
                });
            $cordovaOauth.facebook("833809480073045", ["email", "public_profile", "user_friends"]).then(function (result) {
                $localStorage.fbAccessToken = result.access_token;

                // Get data from FB and store it in our db

                //var userData;
                $http.get("https://graph.facebook.com/v2.4/me", {
                    params: {
                        access_token: $localStorage.fbAccessToken,
                        fields: "name,email",
                        format: "json"
                    }
                })
                    .then(function (result2) {

                        Users.create({
                            name: result2.data.name,
                            email: result2.data.email,
                            access_token: result.access_token
                        }).then(function (response) {

                            if (response.status == 400)
                                alert(response.data);
                            else if (response.status == 200) {
                                $localStorage.user = response.data;
                                //broadcast the event to all child scopes
                                $rootScope.$broadcast('login', { data: response.data });
                                console.log("Broadcast 'login' ");
                                JoinEvent();
                                // $ionicLoading.hide();
                                // $state.go('eventCode');
                            }

                        }, function (err) {
                            $ionicLoading.hide();
                            console.log(err);
                        });

                    }, function (error) {
                        $ionicLoading.hide();
                        alert("There was a problem getting your profile.  Check the logs for details.");
                        console.log(error);
                    });
                //var userData=FB_Google.fbData();

            }, function (error) {
                $ionicLoading.hide();
                console.log(error);
            });
        };

        $scope.googleSignup = function () {
            $ionicLoading.show({
                    content: 'Uploading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200
                    // showDelay: 500
                });
            $cordovaOauth.google("1049642798069-nve6u18q44c573edf7hir5emsg1a4plh.apps.googleusercontent.com", ["https://www.googleapis.com/auth/plus.login", "https://www.googleapis.com/auth/userinfo.email"]).then(function (result) {
                $localStorage.googleAccessToken = result.access_token;

                // Get data from Google and store it in our db
                //        var userData=FB_Google.googleData;
                $http.get("https://www.googleapis.com/plus/v1/people/me", {
                    params: {
                        access_token: $localStorage.googleAccessToken,
                        fields: "displayName,emails",
                        format: "json"
                    }
                })
                    .then(function (result2) {

                        Users.create({
                            name: result2.data.displayName,
                            email: result2.data.emails[0].value,
                            access_token: result.access_token
                        }).then(function (response) {
                            if (response.status == 400)
                                alert(response.data);
                            else if (response.status == 200) {
                                $localStorage.user = response.data;
                                //broadcast the event to all child scopes
                                $rootScope.$broadcast('login', { data: "Dummy Data" });
                                console.log("Broadcast 'login' ");
                                JoinEvent();
                                // $ionicLoading.hide();
                                // $state.go('eventCode');
                            }
                        }, function (err) {
                            $ionicLoading.hide();
                            console.log(err);
                        });

                    }, function (error) {
                        $ionicLoading.hide();
                        alert("There was a problem getting your profile.  Check the logs for details.");
                        console.log(error);
                    });
                //var userData=FB_Google.googleData();
            }, function (error) {
                $ionicLoading.hide();
                console.log(error);
            });
        };

    })