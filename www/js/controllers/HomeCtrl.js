angular.module('starter')

    
    .controller('HomeCtrl', function ($scope, $http, $localStorage, $rootScope,FB_Google,Users) {
        $scope.init = function () {
            // updateLocalStorage();
            $scope.user = $localStorage.user;            
            applyTheme($localStorage.event[0].colorTheme);
            $scope.event = $localStorage.event[0];
        };
        
        // Listening Event
        $rootScope.$on('changeEvent', function (event, data) {
            // $scope.init();
            applyTheme($localStorage.event[0].colorTheme);
            $scope.event = $localStorage.event[0];
            console.log("changeEvent event triggered in HomeCtrl");            
        });
        
        // Listening Event
        $rootScope.$on('login', function (event, data) {
            $scope.user = $localStorage.user;
            console.log("Login event triggered in HomeCtrl");
            console.log(data);
        });
        
        function updateLocalStorage()
        {
            if ($localStorage.user._id) {
                Users.getUserById($localStorage.user._id)
                    .then(function (response) {
                        console.log("User updated");
                        $localStorage.user = response.data;
                        $scope.user = response.data;
                    }, function (err) {
                        alert(err);
                    });
            }
        };
        
        function applyTheme(themeColor) {
                var rules = [];
                if (document.styleSheets[2].cssRules)
                    rules = document.styleSheets[2].cssRules;
                else if (document.styleSheets[2].rules)
                    rules = document.styleSheets[2].rules;

                rules[0].style.backgroundColor = themeColor;
                rules[1].style.backgroundColor = themeColor;
                rules[2].style.color = getContrastYIQ(themeColor);
                rules[3].style.color = themeColor;
                rules[4].style.color = themeColor;

                function getContrastYIQ(hexcolor) {
                    var r = parseInt(hexcolor.substr(1, 2), 16);
                    var g = parseInt(hexcolor.substr(3, 2), 16);
                    var b = parseInt(hexcolor.substr(5, 2), 16);
                    var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
                    return (yiq >= 128) ? 'black' : 'white';
                }
            }

    })