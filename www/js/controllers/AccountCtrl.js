angular.module('starter')

.controller('AccountCtrl', function ($scope, $state, $localStorage,$rootScope,$stateParams) {
        
        
        // // Listening Event
        $rootScope.$on('login', function (event, data) {
            $scope.setState();
        });
        
        $scope.setState=function(){
            $scope.loggedInFromGoogle = $localStorage.googleAccessToken;
            $scope.loggedInFromFb = $localStorage.fbAccessToken; 
        }
        
        $scope.setState();
        
        $scope.settings = {
            enableFriends: true
        };
        

        $scope.logout = function () {
            $localStorage.$reset();
            // navigator.app.loadUrl("file:///android_asset/www/index.html");
            document.location.href = 'index.html#/signup';
            //$state.go('signup');
        };
        
        $scope.changeEvent = function()
        {
            //$localStorage.$reset($localStorage.user);
            // var user =$localStorage.user;
             delete $localStorage.event;  
             delete $localStorage.role;
            // $localStorage.$reset();
            // $localStorage.user = user;
            
            $state.go('eventCode',$stateParams, {reload: true});
            
        };
    })