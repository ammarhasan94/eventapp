angular.module('starter')

    .controller('CodeCtrl', function ($scope, $state, $localStorage, $ionicHistory, $rootScope, $ionicLoading, Events, EventMember) {


        $scope.sendCode = function (code) {
            $ionicLoading.show({
                content: 'Uploading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200
                // showDelay: 500
            });

                  $ionicHistory.clearHistory();
            

            // //Join event
            Events.joinEvent({userId: $localStorage.user._id,code: code})
                .then(function (response) {
                    console.log(response);
                    $localStorage.role = response.data.role;
                    
                    if (response.status == 200) {
                        
                        // Get Event by Event Code
                        Events.getEventByCode(code)
                            .then(function (response2) {
                                console.log(response2);
                                if (response2.status == 200) {
                                    $localStorage.event = response2.data;
                                                                                
                                    //broadcast the event to all child scopes
                                    $rootScope.$broadcast('changeEvent', { data: "Dummy Data" });
                                    console.log("Broadcast 'changeEvent' ");
                                    $ionicLoading.hide();

                                    $state.go('tab.home');
                                }
                            }, function (err) {
                                console.log(err);
                            });
                            
                            
                    } else if (response.status == 404) {
                        $ionicLoading.hide();
                        alert("Please enter valid Event Code");
                    } else {
                        $ionicLoading.hide();
                        alert("Error joining event");
                    }

                }, function (err) {
                    console.log(err);
                });
        }
    });
