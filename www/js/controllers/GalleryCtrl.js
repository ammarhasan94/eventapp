angular.module('starter')

    .controller('GalleryCtrl', function ($scope, $state, $localStorage, $rootScope, $ionicLoading, $cordovaNetwork, Gallery, Like, Dislike) {
        
        $scope.state={
            isOffline:false,
            role : $localStorage.role
        };
        $scope.imagesShown = 21;
        $scope.images = [];
        $scope.allImages = [];
        //Code if we want to get others like as well
        // $scope.likes=[];
        
        // Listening Event
        $rootScope.$on('changeEvent', function (event, data) {
            $scope.loadImages();
            $scope.state.role=$localStorage.role;
        });
        // listen for Online event
        $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
            $scope.state.isOffline=false;
                console.log($scope.state.isOffline);               
            $scope.loadImages();
        });
        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
            $scope.state.isOffline=true;
        });

        $scope.loadImages = function () {
            $scope.state.isOffline = $cordovaNetwork.isOffline();
            console.log($scope.state.isOffline);                

            if ($scope.state.isOffline) {
                console.log("Offline");
            }
            else {
                $ionicLoading.show({
                    content: 'Downloading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200
                    // showDelay: 500
                });
                if ($localStorage.role == "default") {
                    Gallery.getApprovedPictures($localStorage.event[0]._id)
                        .then(function (response) {
                            $ionicLoading.hide();
                            $scope.allImages = response;
                            $scope.images = $scope.allImages.slice(0, $scope.imagesShown);
                            
                            //Code if we want to get others like as well
                            // (function () {
                            //     var index = 0;
                            //     function forloop() {
                            //         if (index < $scope.images.length) {
                            //             Like.getByPictureId($scope.images[index]._id)
                            //                 .then(function (response) {
                            //                     console.log("Likes for image: " + index);
                            //                     console.log(response.data);
                            //                     $scope.likes.push(response.data);
                            //                     Like.pushToAllLikes(response.data);
                            //                     index++;
                            //                     forloop();
                            //                 }, function (err) {
                            //                     alert(err);
                            //                 });
                            //         } else {
                            //             console.log("all done");
                            //         }
                            //     }
                            //     forloop();
                            // })();
                                                   
                        }, function (err) {
                            $ionicLoading.hide();
                            alert(err);
                        });
                } else {
                    Gallery.getAllPictures($localStorage.event[0]._id)
                        .then(function (response) {
                            $ionicLoading.hide();
                            $scope.allImages = response;
                            $scope.images = $scope.allImages.slice(0, $scope.imagesShown);
                            
                            //Code if we want to get others like as well
                            //   (function () {
                            //     var index = 0;
                            //     function forloop() {
                            //         if (index < $scope.images.length) {
                            //             Like.getByPictureId($scope.images[index]._id)
                            //                 .then(function (response) {
                            //                     console.log("Likes for image: " + index);
                            //                     console.log(response.data);
                            //                     $scope.likes.push(response.data);
                            //                     Like.pushToAllLikes(response.data);

                            //                     index++;
                            //                     forloop();
                            //                 }, function (err) {
                            //                     alert(err);
                            //                 });
                            //         } else {
                            //             console.log("all done");
                            //         }
                            //     }
                            //     forloop();
                            // })();
                            
                        }, function (err) {
                            $ionicLoading.hide();
                            alert(err);
                        });
                }
            }
        }

        $scope.moreDataCanBeLoaded = true;
        $scope.loadMore = function () {
            console.log("loadMore called");
            if ($scope.allImages.length > $scope.images.length) {
                var newImages = $scope.allImages.slice($scope.imagesShown, 6);
                console.log(newImages);
                //Code if we want to get others like as well
                    
                // (function () {
                //         var index = 0;
                //         function forloop() {
                //             if (index < $scope.newImages.length) {
                //                 $scope.images.push(newImages[index]);
                                        
                //                 Like.getByPictureId(newImages[index]._id)
                //                     .then(function (response) {
                //                         console.log("Likes for image: " + index);
                //                         console.log(response.data);
                //                         $scope.likes.push(response.data);
                //                         Like.pushToAllLikes(response.data);

                //                         index++;
                //                         forloop();
                //                     }, function (err) {
                //                         alert(err);
                //                     });
                //             } else {
                //                 console.log("all done");
                //             }
                //         }
                //         forloop();
                //     })();
                            
            } else {
                $scope.moreDataCanBeLoaded = false;
            }
            $scope.$broadcast('scroll.infiniteScrollComplete');
        }

        $scope.selectImage = function (index) {
            console.log("selectImage: " + index);
            Gallery.setSelected(index);
            $state.go('tab.gallery-detail');
        };
    })