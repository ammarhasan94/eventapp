    angular.module('starter')
        
        .controller('CameraCtrl', function ($scope, $localStorage, $timeout, $ionicLoading, Camera, Events,CLOUDINARY) {
     
            $scope.image = {
                description:""
            }; 
            
            $scope.getPhoto = function () {
                Camera.getPicture().then(function (imageURI) {
                    $scope.lastPhoto = imageURI;   
                    $scope.image.description="";
                }, function (err) {
                    console.err(err);
                    alert(err);
                }, {
                        quality: 10,
                        targetWidth: 20,
                        targetHeight: 80,
                        destinationType: navigator.camera.DestinationType.FILE_URI,
                        sourceType: navigator.camera.PictureSourceType.CAMERA,
                        encodingType: navigator.camera.EncodingType.JPEG,
                    });
            };
            
            $scope.upload = function () {

                function win(r) {
                    console.log("Response");
                    var response = JSON.parse(r.response);
                    console.log(response);
                    console.log("URL:" + response.url);
                    console.log("Sent = " + r.bytesSent);

                    Events.createPicture(response.url, $localStorage.user._id, $localStorage.event[0]._id,$scope.image.description)
                        .then(function (pic) {
                            $ionicLoading.hide();
                            console.log(pic);
                            alert("Uploaded");
                            $scope.lastPhoto=false;
                        }, function (err) {
                            console.log(err);
                            $ionicLoading.hide();
                        });
                }

                function fail(error) {
                    alert("An error has occurred: Code = " + error.code);
                    console.log("upload error source " + error.source);
                    console.log("upload error target " + error.target);
                }

                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.fileName = $scope.lastPhoto.substr($scope.lastPhoto.lastIndexOf('/') + 1);
                options.mimeType = "image/jpeg";
                var params = {};
                params.upload_preset = CLOUDINARY.UPLOAD_PRESET;
                options.params = params;

                var ft = new FileTransfer();
                ft.upload($scope.lastPhoto, encodeURI(CLOUDINARY.URL), win, fail, options, true);

                $ionicLoading.show({
                    content: 'Uploading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200
                    // showDelay: 500
                });
            }
        })