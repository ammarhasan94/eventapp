angular.module('starter')

    .controller('PhotoCtrl', function ($scope, $state, $localStorage, Gallery, Like, Dislike) {
        //$scope.image = Gallery.getSelected();

//Code if we want to get others like as well
        // var likesOnCurrentPicture = [];
        
        $scope.images = Gallery.getLoadedPictures();
        $scope.activeSlide = Gallery.getSelected();
        $scope.role = $localStorage.role;
        // $scope.allLikes = Gallery.getAllLikes();
        
        // This object 'state' is to solve $scope issues
        $scope.state={
            isLiked:false,
            isDisliked:false,
            isApproved:$scope.images[$scope.activeSlide].isApproved
        };
        // updateLikeDislikeCount();
        updateLikeAndDislike();
        
        $scope.approve = function () {
            console.log("Called approve : should return true");
            $scope.state.isApproved = true;
   
            Gallery.approvePicture($localStorage.user._id, $scope.images[$scope.activeSlide]._id)
                .then(function (response) {
                    console.log(response);
                    $scope.state.isApproved = response.data;
                    $scope.images[$scope.activeSlide].isApproved=true;
                }, function (err) {
                    console.log(err);
                    $scope.state.isApproved = false;
                });
        };

        $scope.disapprove = function () {
            console.log("Called disapprove : should return false");
            $scope.state.isApproved = false;
          
            Gallery.disapprovePicture($localStorage.user._id, $scope.images[$scope.activeSlide]._id)
                .then(function (response) {
                    console.log(response);
                    $scope.state.isApproved = response.data;
                    $scope.images[$scope.activeSlide].isApproved=false;
                }, function (err) {
                    console.log(err);
                    $scope.state.isApproved = true;
                });
        };

//Code if we want to get others like as well
        // likesOnCurrentPicture = $scope.allLikes[$scope.activeSlide];
        // console.log("likesOnCurrentPicture");
        // console.log(likesOnCurrentPicture);
        
        // for (var i = 0; i < likesOnCurrentPicture.length; i++) {
        //     var like = likesOnCurrentPicture[i];
        //     if(like["userId"]==$localStorage.user._id){
        //         $scope.state.isLiked=true;                
        //         break;
        //     }
        // }
        
        $scope.like = function () {
            //Change UI before any request
            $scope.state.isLiked = true;
            
            //If we are liking a picture which is disliked, first we have to delete dislike  
            if ($scope.state.isDisliked) {
                Dislike.delete($scope.images[$scope.activeSlide]._id, $localStorage.user._id)
                    .then(function (response) {
                        console.log("Delete dislike called while Liking");
                        console.log(response);
                        if (response.data == "") {
                            $scope.state.isDisliked = true;
                        } else {
                            $scope.state.isDisliked = false;
                        }
                    }, function (err) {
                        console.log(err);
                        $scope.state.isDisliked = true;
                    });
            }
            Like.create($scope.images[$scope.activeSlide]._id, $localStorage.user._id)
                .then(function (response) {
                    console.log("Create Like");
                    console.log(response);
                    if (response.data.length == 0) {
                        $scope.state.isLiked = false;
                    } else {
                        $scope.state.isLiked = true;
                    }

                    updateLikeDislikeCount();                    
                    
                }, function (err) {
                    console.log(err);
                    $scope.state.isLiked = false;
                });
        }
        $scope.deleteLike=function(){
            //Change UI before any request
            $scope.state.isLiked = false;
            
            Like.delete($scope.images[$scope.activeSlide]._id,$localStorage.user._id)
            .then(function (response) {
                    console.log("Delete Like");                
                    console.log(response);
                    if(response.data==""){
                        $scope.state.isLiked =true;
                    }else{
                        $scope.state.isLiked =false;
                    }
                    
                    updateLikeDislikeCount();                    
                    
                }, function (err) {
                    console.log(err);
                    $scope.state.isLiked =true;
                });
        }
        $scope.dislike = function () {
            //Change UI before any request
            $scope.state.isDisliked = true;
            
            //If we are disliking a picture which is liked, first we have to delete like  
            if ($scope.state.isLiked) {
                Like.delete($scope.images[$scope.activeSlide]._id, $localStorage.user._id)
                    .then(function (response) {
                        console.log("Delete Like called while Disliking ");
                        console.log(response);
                        if (response.data == "") {
                            $scope.state.isLiked = true;
                        } else {
                            $scope.state.isLiked = false;
                        }
                    }, function (err) {
                        console.log(err);
                        $scope.state.isLiked = true;
                    });
            }
            Dislike.create($scope.images[$scope.activeSlide]._id, $localStorage.user._id)
                .then(function (response) {
                    console.log("Create Dislike");
                    console.log(response);
                    if (response.data.length == 0) {
                        $scope.state.isDisliked = false;
                    } else {
                        $scope.state.isDisliked = true;
                    }

                    updateLikeDislikeCount();                    

                }, function (err) {
                    console.log(err);
                    $scope.state.isDisliked = false;
                });
        }
        $scope.deleteDislike=function(){
            //Change UI before any request
            $scope.state.isDisliked = false;
            
            Dislike.delete($scope.images[$scope.activeSlide]._id,$localStorage.user._id)
            .then(function (response) {
                    console.log("Delete Dislike");                
                    console.log(response);
                    if(response.data==""){
                        $scope.state.isDisliked =true;
                    }else{
                        $scope.state.isDisliked =false;
                    }
             
                    updateLikeDislikeCount();                                        
             
                }, function (err) {
                    console.log(err);
                    $scope.state.isLiked =true;
                });
        }
        
        function updateLikeDislikeCount() {
            //Get Likes Count
            Like.getCount($scope.images[$scope.activeSlide]._id).then(
                function (response2) {
                    console.log(response2);
                    $scope.images[$scope.activeSlide].likeCount = response2.data;
                },
                function (err2) {
                    console.log(err2);
                });
            //Get Dislikes Count
            Dislike.getCount($scope.images[$scope.activeSlide]._id).then(
                function (response2) {
                    console.log(response2);
                    $scope.images[$scope.activeSlide].dislikeCount = response2.data;
                },
                function (err2) {
                    console.log(err2);
                });

        }
        
        //This function also calls 'updateLikeDislikeCount' in the end
        function updateLikeAndDislike() {
            Like.getByPictureIdAndUserId($scope.images[$scope.activeSlide]._id, $localStorage.user._id)
                .then(function (response) {
                    console.log(response);
                    if (response.data == "") {
                        $scope.state.isLiked = false;
                    } else {
                        $scope.state.isLiked = true;
                    }
                                
                }, function (err) {
                    console.log(err);
                });
            Dislike.getByPictureIdAndUserId($scope.images[$scope.activeSlide]._id, $localStorage.user._id)
                .then(function (response) {
                    console.log(response);
                    if (response.data == "") {
                        $scope.state.isDisliked = false;
                    } else {
                        $scope.state.isDisliked = true;
                    }
                
                    updateLikeDislikeCount();                    
                
                }, function (err) {
                    console.log(err);
                });
        };
        
        $scope.slideChanged = function (index) {
            Gallery.setSelected(index);
            $scope.activeSlide = index;
            $scope.state.isApproved = $scope.images[$scope.activeSlide].isApproved;
            updateLikeAndDislike();
            console.log($scope.activeSlide);
            console.log($scope.state.isApproved);
        };
        
    })
