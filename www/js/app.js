// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.services', 'ngCordova', 'ngStorage','angular-spinkit'])

.run(function($ionicPlatform,$state,$localStorage) {
    //ngFB.init({appId: '833809480073045'});

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }

    var hasJoinedEvent=$localStorage.hasOwnProperty("event");
    var isSignedin=$localStorage.hasOwnProperty("user");
    
    if( isSignedin && hasJoinedEvent ){
      $state.go('tab.home');
    }
    else if(isSignedin && !hasJoinedEvent) {
      $state.go('signup');
    }
    else
      $state.go('signup');
    // $state.go('tab.home');
    //$state.go('eventCode');
    // $state.go('tab.gallery');      
  });
})
//$compileProvider added with Camera Functionality
.config(function($stateProvider, $urlRouterProvider,$compileProvider) {
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
    // $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider


  .state('signup',{
        url:'/signup',
        templateUrl:'templates/signup.html',
        controller:'SignupCtrl'
  })
  .state('login',{
        url:'/login',
        templateUrl:'templates/login.html',
        controller:'LoginCtrl'
  })
    .state('eventCode',{
      url:'/code',
      templateUrl:'templates/event-code.html',
      controller:'CodeCtrl'
    })
   // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:
 .state('tab.home', {
    url: '/home',
    views: {
      'tab-home': {
        templateUrl: 'templates/tab-home.html',
        controller: 'HomeCtrl'
      }
    }
  })

  .state('tab.camera', {
    url: '/camera',
    views: {
      'tab-camera': {
        templateUrl: 'templates/tab-camera.html',
        controller: 'CameraCtrl'
      }
    }
  })
  .state('tab.gallery', {
    url: '/gallery',
    views: {
      'tab-gallery': {
        templateUrl: 'templates/tab-gallery.html',
        controller: 'GalleryCtrl'
      }
    }
  })
    .state('tab.gallery-detail', {
      url: '/gallery/:photoId',
      views: {
        'tab-gallery': {
          templateUrl: 'templates/gallery-detail.html',
          controller: 'PhotoCtrl'
        }
      }
    })
    .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});
